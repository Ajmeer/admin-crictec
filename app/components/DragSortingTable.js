/* eslint-disable prefer-destructuring */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/no-multi-comp */
/* eslint-disable no-param-reassign */
import React from 'react';
import PropTypes from 'prop-types';
import { Table, Popconfirm, Button, Tag, Select } from 'antd';
import { DragDropContext, DragSource, DropTarget } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
// import update from 'immutability-helper';

const Option = Select.Option;

function dragDirection(
  dragIndex,
  hoverIndex,
  initialClientOffset,
  clientOffset,
  sourceClientOffset,
) {
  const hoverMiddleY = (initialClientOffset.y - sourceClientOffset.y) / 2;
  const hoverClientY = clientOffset.y - sourceClientOffset.y;
  if (dragIndex < hoverIndex && hoverClientY > hoverMiddleY) {
    return 'downward';
  }
  if (dragIndex > hoverIndex && hoverClientY < hoverMiddleY) {
    return 'upward';
  }
  return null;
}

class BodyRow extends React.Component {
  render() {
    const {
      isOver,
      connectDragSource,
      connectDropTarget,
      moveRow,
      dragRow,
      clientOffset,
      sourceClientOffset,
      initialClientOffset,
      ...restProps
    } = this.props;
    const style = { ...restProps.style, cursor: 'move' };

    let { className } = restProps;
    if (isOver && initialClientOffset) {
      const direction = dragDirection(
        dragRow.index,
        restProps.index,
        initialClientOffset,
        clientOffset,
        sourceClientOffset,
      );
      if (direction === 'downward') {
        className += ' drop-over-downward';
      }
      if (direction === 'upward') {
        className += ' drop-over-upward';
      }
    }

    return connectDragSource(
      connectDropTarget(
        <tr {...restProps} className={className} style={style} />,
      ),
    );
  }
}

const rowSource = {
  beginDrag(props) {
    return {
      index: props.index,
    };
  },
};

const rowTarget = {
  drop(props, monitor) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }

    // Time to actually perform the action
    props.moveRow(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
  },
};

const DragableBodyRow = DropTarget('row', rowTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  sourceClientOffset: monitor.getSourceClientOffset(),
}))(
  DragSource('row', rowSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    dragRow: monitor.getItem(),
    clientOffset: monitor.getClientOffset(),
    initialClientOffset: monitor.getInitialClientOffset(),
  }))(BodyRow),
);

class DragSortingTable extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     data: this.props.data,
  //   };
  //   console.log(this.props);
  // }

  components = {
    body: {
      row: DragableBodyRow,
    },
  };

  // moveRow = (dragIndex, hoverIndex) => {
  //   const { data } = this.state;
  //   const dragRow = data[dragIndex];
  //   this.setState(
  //     update(this.state, {
  //       data: {
  //         $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]],
  //       },
  //     }),
  //   );
  // };

  render() {
    const columns = [
      {
        title: 'Priority',
        // dataIndex: 'priority',
        // key: 'priority',
        render: (data, obj, index) => <span>{index + 1}</span>,
      },
      {
        title: 'Series',
        dataIndex: 'seriesName',
        key: 'seriesName',
      },
      {
        title: 'Match',
        dataIndex: 'matchName',
        key: 'matchName',
      },
      {
        title: 'BBB Feed',
        dataIndex: 'bBBFeed',
        key: 'bBBFeed',
        render: (data, obj, index) => (
          <Select key={data} style={{ width: '90%' }} defaultValue={data}>
            {obj.feedList &&
              obj.feedList.map(feed => (
                <Option key={`${feed}_${index}`} value={feed}>
                  {feed}
                </Option>
              ))}
          </Select>
        ),
      },
      {
        title: 'Status',
        dataIndex: 'status',
        render: data => (
          <Tag
            color={
              data === 'LIVE' ? 'green' : data === 'UPCOMING' ? 'red' : 'purple'
            }
          >
            {data}
          </Tag>
        ),
        // key: 'matchStatus',
      },
      {
        title: 'Win % Feed',
        dataIndex: 'winFeed',
        key: 'winFeed',
        render: data => (
          <Select style={{ width: '90%' }} defaultValue={data}>
            <Option value="Cricviz">Cricviz</Option>
          </Select>
        ),
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record, index) => (
          <Popconfirm
            placement="top"
            title="Are you sure to delete this Match"
            onConfirm={() => this.props.confirmDeleteMatch(index)}
            okText="Yes"
            cancelText="No"
          >
            <Button type="danger" icon="delete" />
          </Popconfirm>
        ),
      },
    ];
    return (
      <div className="score-table">
        <Table
          columns={columns}
          dataSource={this.props.data}
          components={this.components}
          pagination={false}
          onRow={(record, index) => ({
            index,
            moveRow: this.props.moveRow,
            // onMouseEnter: this.rowMouseOver,
          })}
        />
      </div>
    );
  }
}

DragSortingTable.propTypes = {
  confirmDeleteMatch: PropTypes.func,
  moveRow: PropTypes.func,
  data: PropTypes.array,
};

export default DragDropContext(HTML5Backend)(DragSortingTable);
