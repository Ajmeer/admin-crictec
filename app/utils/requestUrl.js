const BASE_APP_URL = 'http://13.233.193.187/api/admin/';

export const API_GET_SERIES = {
  url: `${BASE_APP_URL}get-scoring-series`,
  method: 'GET',
};

export const API_GET_MATCHES_BY_SERIES = seriesId => ({
  url: `${BASE_APP_URL}get-matches-for-series/${seriesId}`,
  method: 'GET',
});

export const API_GET_FEED_ORIGIN = matchId => ({
  url: `${BASE_APP_URL}get-feed-source/${matchId}`,
  method: 'GET',
});

export const API_GET_PREDICTION_ORIGIN = {
  url: `${BASE_APP_URL}get-win-feed`,
  method: 'GET',
};

export const API_GET_SCORING_DATA = {
  url: `${BASE_APP_URL}get-scoring-data`,
  method: 'GET',
};

export const API_POST_SCORING_DATA = {
  url: `${BASE_APP_URL}save-score-details`,
  method: 'POST',
};
