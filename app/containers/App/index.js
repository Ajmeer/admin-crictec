/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import 'assets/scss/main.scss';

// import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Scoring from 'containers/Scoring';
import TeamProfile from 'containers/TeamProfile';
import Landing from 'containers/Landing';
import HomeScreenEditor from 'containers/HomeScreenEditor';
import WrappedPlayerProfileForm from 'containers/Player';
import Stadium from 'containers/Stadium';
import RankingRecords from 'containers/RankingRecords';
import PollsDetail from 'containers/PollsDetail';
import Notifications from 'containers/Notification';
import Photos from 'containers/Photos';
import Header from './Header';

export default function App({ history }) {
  return (
    <React.Fragment>
      <Header history={history} />
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route exact path="/scoring" component={Scoring} />
        <Route exact path="/console" component={Landing} />
        <Route exact path="/homescreen" component={HomeScreenEditor} />
        <Route
          exact
          path="/player-profile"
          component={WrappedPlayerProfileForm}
        />
        <Route exact path="/team" component={TeamProfile} />
        <Route exact path="/stadium-profile" component={Stadium} />
        <Route exact path="/ranking-records" component={RankingRecords} />
        <Route exact path="/polls" component={PollsDetail} />
        <Route exact path="/photos" component={Photos} />
        <Route exact path="/notification" component={Notifications} />
        <Route component={NotFoundPage} />
      </Switch>
    </React.Fragment>
  );
}

App.propTypes = {
  history: PropTypes.object,
};
