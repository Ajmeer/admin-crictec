/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Layout, Menu, Dropdown, Avatar } from 'antd';
const { Header } = Layout;

const Headers = ({ history }) => (
  <React.Fragment>
    <Header
      style={{
        background: '#F8F8F8',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <div className="logo">
        <img
          style={{ cursor: 'pointer' }}
          src={require('images/Logo.png')}
          alt="Logo"
          onClick={() => history.push('/')}
        />
      </div>
      <Dropdown
        style={{ color: '#000' }}
        overlay={
          <Menu>
            <Menu.Item>
              <a href="#">Logout</a>
            </Menu.Item>
          </Menu>
        }
      >
        <a className="ant-dropdown-link" href="#">
          {/* username <Icon type="down" /> */}
          <Avatar icon="user" />
        </a>
      </Dropdown>
    </Header>
  </React.Fragment>
);

Headers.propTypes = {
  history: PropTypes.object,
};

export default withRouter(Headers);
