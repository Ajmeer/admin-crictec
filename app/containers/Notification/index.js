/* eslint-disable no-console */
/**
 *
 * Notification
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Layout, Form, Input, Button } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import TextArea from 'antd/lib/input/TextArea';
import makeSelectNotification from './selectors';
import reducer from './reducer';
import saga from './saga';

const { Content } = Layout;

/* eslint-disable react/prefer-stateless-function */
class Notification extends React.Component {
  handleSubmit = ev => {
    ev.preventDefault();
    console.log(ev);
    this.props.form.validateFields((err, fieldsValue) => {
      // if (err) {
      //   return;
      // }
      console.log(err, fieldsValue);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Layout className="layout">
        <Content className="main">
          {/* =================== */}
          {/* Body Section Starts */}
          <div className="page">
            <h1 className="main__title">Notifications</h1>
            <div className="page__info">
              <Layout style={{ padding: 24, background: '#fff' }}>
                {/* <Input />
              <Input /> */}
                <div style={{ width: '30%', margin: '0 auto' }}>
                  <Form onSubmit={this.handleSubmit}>
                    <Form.Item label="Title">
                      {getFieldDecorator('title', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Title!',
                          },
                        ],
                      })(<Input />)}
                    </Form.Item>
                    <Form.Item label="Description">
                      {getFieldDecorator('description', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Description!',
                          },
                        ],
                      })(<TextArea />)}
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" htmlType="submit">
                        Send
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </Layout>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

Notification.propTypes = {
  form: PropTypes.object,
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  notification: makeSelectNotification(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'notification', reducer });
const withSaga = injectSaga({ key: 'notification', saga });

export default Form.create()(
  compose(
    withReducer,
    withSaga,
    withConnect,
  )(Notification),
);
