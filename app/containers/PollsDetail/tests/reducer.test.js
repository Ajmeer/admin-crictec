import pollsDetailReducer from '../reducer';

describe('pollsDetailReducer', () => {
  it('returns the initial state', () => {
    expect(pollsDetailReducer(undefined, {})).toEqual({});
  });
});
