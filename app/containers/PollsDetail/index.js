/* eslint-disable no-console */
/**
 *
 * PollsDetail
 *
 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */
import React from 'react';
// import PropTypes from 'prop-types';
import {
  Tabs,
  Layout,
  Table,
  Button,
  Form,
  Modal,
  Icon,
  Select,
  Input,
  Row,
  Col,
} from 'antd';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPollsDetail from './selectors';
import reducer from './reducer';
import saga from './saga';
const { Content } = Layout;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const columns1 = [
  {
    title: 'S.no',
    dataIndex: 'sno',
  },
  {
    title: 'Poll ID',
    className: 'pollId',
    dataIndex: 'id',
  },
  {
    title: 'Start Date and Time',
    dataIndex: 'startDate',
  },
  {
    title: 'End Date and Time',
    dataIndex: 'endDate',
  },
  {
    title: 'Match',
    dataIndex: 'match',
  },
  {
    title: 'Home page',
    dataIndex: 'homePage',
  },
  {
    title: 'Edit',
    dataIndex: 'edit',
  },
];

const data1 = [
  {
    key: '1',
    sno: '1',
    id: '12323',
    startDate: '16/01/2019 11:20',
    endDate: '16/01/2019 11:45',
    match: 'India vs Aus 2nd ODI',
    homePage: 'Yes',
    edit: 'End Poll',
  },
  {
    key: '2',
    sno: '2',
    id: '12453',
    startDate: '16/01/2019 11:30',
    endDate: '16/01/2019 11:50',
    match: 'India vs Aus 2nd ODI',
    homePage: 'No',
    edit: 'End Poll',
  },
];
const columns2 = [
  {
    title: 'S.no',
    dataIndex: 'sno',
  },
  {
    title: 'Poll ID',
    className: 'pollId',
    dataIndex: 'id',
  },
  {
    title: 'Start Date and Time',
    dataIndex: 'startDate',
  },
  {
    title: 'End Date and Time',
    dataIndex: 'endDate',
  },
  {
    title: 'Match',
    dataIndex: 'match',
  },
  {
    title: ' Display on Home page',
    dataIndex: 'homePage',
  },
  {
    title: 'Result',
    dataIndex: 'result',
  },
];

const data2 = [
  {
    key: '1',
    sno: '1',
    id: '12323',
    startDate: '16/01/2019 11:20',
    endDate: '16/01/2019 11:45',
    match: 'India vs Aus 2nd ODI',
    homePage: 'Yes',
    result: 'See Result',
  },
  {
    key: '2',
    sno: '2',
    id: '12453',
    startDate: '16/01/2019 11:30',
    endDate: '16/01/2019 11:50',
    match: 'India vs Aus 2nd ODI',
    homePage: 'No',
    result: 'See Result',
  },
];
// Select box
const Option = Select.Option;
// Form
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
/* eslint-disable react/prefer-stateless-function */
export class PollsDetail extends React.Component {
  state = { visible: false };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <Layout className="layout">
        {/* =================== */}
        {/* Body Section Starts */}
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Polls</h1>
            <div className="page__info">
              <Tabs defaultActiveKey="1">
                <TabPane tab="Poll" key="1">
                  <h3 style={{ padding: '20px 0px' }}>Active Polls</h3>
                  <Table
                    className="pollTable"
                    columns={columns1}
                    dataSource={data1}
                    bordered
                  />
                  <Button
                    type="primary"
                    onClick={this.showModal}
                    style={{ margin: '20px 0px 0px' }}
                  >
                    Add Poll
                  </Button>
                  <Modal
                    title="Add polls"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={null}
                  >
                    <div className="polls_detail">
                      <Form>
                        <Row gutter={24}>
                          <Col span={8}>
                            <Form.Item label="Poll Id" {...formItemLayout}>
                              <Input
                                placeholder="Name"
                                style={{ width: 100 }}
                              />
                            </Form.Item>
                          </Col>
                          <Col span={8}>
                            <Form.Item label="Start" {...formItemLayout}>
                              <Input
                                placeholder="Name"
                                style={{ width: 100 }}
                              />
                            </Form.Item>
                          </Col>
                          <Col span={8}>
                            <Form.Item label="Match" {...formItemLayout}>
                              <Input
                                placeholder="Name"
                                style={{ width: 100 }}
                              />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row gutter={24}>
                          <Col span={8}>
                            {/* <Form.Item label="Poll Id" {...formItemLayout}>
                          <Input placeholder="Name" style={{ width: 100 }} />
                        </Form.Item> */}
                          </Col>
                          <Col span={8}>
                            <Form.Item label="End" {...formItemLayout}>
                              <Input
                                placeholder="Name"
                                style={{ width: 100 }}
                              />
                            </Form.Item>
                          </Col>
                          <Col span={8}>
                            <Form.Item label="Display" {...formItemLayout}>
                              <Select
                                defaultValue="Both"
                                suffixIcon={<Icon type="caret-down" />}
                                style={{ width: 100 }}
                              >
                                <Option value="Cricket API">
                                  Trending Players
                                </Option>
                                <Option value="Opta C2">
                                  Trending Players
                                </Option>
                              </Select>
                            </Form.Item>
                          </Col>
                        </Row>

                        <Row>
                          <Col span={18}>
                            <Form.Item label="Question" {...formItemLayout}>
                              <TextArea
                                placeholder="Autosize height with minimum and maximum number of lines"
                                autosize={{ minRows: 2, maxRows: 6 }}
                                cols="30"
                              />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col span={18}>
                            <Form.Item label="option 1" {...formItemLayout}>
                              <Input placeholder="" style={{ width: 250 }} />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col span={18}>
                            <Form.Item label="option 2" {...formItemLayout}>
                              <Input placeholder="" style={{ width: 250 }} />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col span={18}>
                            <Form.Item label="option 3" {...formItemLayout}>
                              <Input placeholder="" style={{ width: 250 }} />
                            </Form.Item>
                          </Col>
                        </Row>
                        <Row>
                          <Button type="primary">Save</Button>
                        </Row>
                      </Form>
                    </div>
                  </Modal>
                  <h3 style={{ padding: '20px 0px' }}>Archive Polls</h3>
                  <Table
                    className="pollTable"
                    columns={columns2}
                    dataSource={data2}
                    bordered
                  />
                </TabPane>
                <TabPane tab="Quick bites" key="2">
                  <div style={{ width: '30%', margin: '0 auto' }}>
                    <Form>
                      <Form.Item label="Title">
                        <Input />
                      </Form.Item>
                      <Form.Item label="Description">
                        <Input />
                      </Form.Item>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">
                          Send
                        </Button>
                      </Form.Item>
                    </Form>
                  </div>
                </TabPane>
              </Tabs>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

PollsDetail.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pollsdetail: makeSelectPollsDetail(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'pollsDetail', reducer });
const withSaga = injectSaga({ key: 'pollsDetail', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PollsDetail);
