import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pollsDetail state domain
 */

const selectPollsDetailDomain = state => state.pollsDetail || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PollsDetail
 */

const makeSelectPollsDetail = () =>
  createSelector(selectPollsDetailDomain, substate => substate);

export default makeSelectPollsDetail;
export { selectPollsDetailDomain };
