import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the scoring state domain
 */

const selectScoringDomain = state => state.scoring || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Scoring
 */

const makeSelectScoring = () =>
  createSelector(selectScoringDomain, substate => substate);

export default makeSelectScoring;
export { selectScoringDomain };
