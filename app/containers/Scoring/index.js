/* eslint-disable no-param-reassign */
/* eslint-disable dot-notation */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable prefer-destructuring */
/**
 *
 * Scoring
 *
 */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Button, Form, Select, Modal, message } from 'antd';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import update from 'immutability-helper';

import DragSortingTable from 'components/DragSortingTable';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectScoring from './selectors';
import reducer from './reducer';
import saga from './saga';

import * as scoringActions from './actions';
const { Content } = Layout;
const Option = Select.Option;

class Scoring extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      confirmLoading: false,
      isMatchSelected: false,
      data: [],
      initial: false,
    };
    this.scoreData = [];
    this.scoreObj = {};
  }

  componentDidMount = () => {
    this.props.getfeaturedSeries();
    this.props.getScoring();
  };

  static getDerivedStateFromProps = (nextProps, state) => {
    if (nextProps.scoring.featuredMatches.length > 0 && !state.initial) {
      return {
        data: nextProps.scoring.featuredMatches,
        initial: true,
      };
    }
    if (nextProps.scoring.postScoringSuccess) {
      message.success('Scoring successfully created');
      nextProps.history.push('/');
    }
    return { ...state };
  };

  add = () => {
    this.setState({
      visible: !this.state.visible,
    });
  };

  handleCancel = () => {
    this.props.form.setFieldsValue({
      seriesId: { key: '', label: '' },
    });
    this.props.form.setFieldsValue({
      matchId: { key: '', label: '' },
    });
    this.setState(
      {
        visible: !this.state.visible,
        isMatchSelected: false,
      },
      () => this.props.resetMatches(),
    );
  };

  handleOk = () => {
    this.props.form.validateFields(err => {
      if (!err) {
        this.props.form.setFieldsValue({
          seriesId: { key: '', label: '' },
        });
        this.props.form.setFieldsValue({
          matchId: { key: '', label: '' },
        });
        const score = { ...this.scoreObj };
        // score['priority'] = this.scoreData.length + 1;
        this.scoreData = [...this.state.data];
        this.scoreData.push(score);
        this.setState(
          {
            visible: !this.state.visible,
            isMatchSelected: false,
            data: this.scoreData,
          },
          () => this.props.resetMatches(),
        );
      }
    });
  };

  handleSeriesChange = ev => {
    this.props.getSeriesMatches(ev.key);
    this.scoreObj['seriesId'] = ev.key;
    this.scoreObj['seriesName'] = ev.label;
  };

  handleMatchChange = ev => {
    this.scoreObj['matchId'] = ev.key;
    this.scoreObj['matchName'] = ev.label;
    this.setState(
      {
        isMatchSelected: true,
      },
      () => this.props.getFeeds(ev.key),
    );
  };

  handleFeedChange = ev => {
    this.scoreObj['bBBFeed'] = ev;
    this.scoreObj['feedList'] = this.props.scoring.feeds.bbbFeed;
    this.scoreObj['status'] = this.props.scoring.feeds.status;
  };

  handlePredictSourceChange = ev => {
    this.scoreObj['winFeed'] = ev;
  };

  confirmDeleteMatch = id => {
    this.setState(
      update(this.state, {
        data: {
          $splice: [[id, 1]],
        },
      }),
    );
  };

  saveScoring = () => {
    // console.log(this.state.data);
    const scorePostData = [...this.state.data];
    const postData = scorePostData.map((dt, index) => {
      dt['priority'] = index + 1;
      return dt;
    });
    this.props.saveScoring(postData);
  };

  // table drag function

  moveRow = (dragIndex, hoverIndex) => {
    const { data } = this.state;
    const dragRow = data[dragIndex];
    this.setState(
      update(this.state, {
        data: {
          $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]],
        },
      }),
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { series, matches, feeds } = this.props.scoring;
    return (
      <Layout className="layout">
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Scoring</h1>
            <div className="page__info">
              <Layout style={{ padding: 24, background: '#fff' }}>
                <DragSortingTable
                  data={this.state.data}
                  confirmDeleteMatch={this.confirmDeleteMatch}
                  ref={this.tableRef}
                  moveRow={this.moveRow}
                />
                <Form onSubmit={this.handleSubmit}>
                  <Form.Item>
                    <Button
                      type="primary"
                      onClick={this.add}
                      icon="plus"
                      shape="round"
                    >
                      Add Match
                    </Button>
                    <div style={{ display: 'inline-block', float: 'right' }}>
                      <Button
                        type="primary"
                        style={{ marginRight: '10px' }}
                        onClick={() => this.saveScoring()}
                      >
                        Save
                      </Button>
                      <Button
                        type="danger"
                        onClick={() => this.props.history.push('/')}
                      >
                        Discard
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </Layout>
            </div>
          </div>
        </Content>
        <Modal
          title="Add Match"
          visible={this.state.visible}
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
          maskClosable={false}
        >
          <Form layout="vertical">
            <Form.Item label="Series">
              {getFieldDecorator('seriesId', {
                rules: [
                  {
                    required: true,
                    message: 'Please select any series!',
                  },
                ],
              })(
                <Select onChange={this.handleSeriesChange} labelInValue>
                  {series &&
                    series.map(ser => (
                      <Option value={ser.crictecId}>{ser.name}</Option>
                    ))}
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="Match">
              {getFieldDecorator('matchId', {
                rules: [
                  {
                    required: true,
                    message: 'Please select any Match!',
                  },
                ],
              })(
                <Select
                  labelInValue
                  disabled={matches && matches.length === 0}
                  loading={this.props.seriesMatchesLoading}
                  onChange={this.handleMatchChange}
                >
                  {matches &&
                    matches.map(match => (
                      <Option value={match.crictecId}>{match.name}</Option>
                    ))}
                </Select>,
              )}
            </Form.Item>
            {this.state.isMatchSelected && (
              <React.Fragment>
                <Form.Item label="BBB Feed">
                  {getFieldDecorator('feed', {
                    rules: [
                      {
                        required: true,
                        message: 'Please select any BBB Feed Source!',
                      },
                    ],
                  })(
                    <Select
                      disabled={feeds.length === 0}
                      loading={this.props.scoring.feedsLoading}
                      onChange={this.handleFeedChange}
                    >
                      {feeds &&
                        feeds.bbbFeed &&
                        feeds.bbbFeed.map(feed => (
                          <Option value={feed}>{feed}</Option>
                        ))}
                    </Select>,
                  )}
                </Form.Item>
                <Form.Item label="Win % Feed">
                  {getFieldDecorator('winFeed', {
                    rules: [
                      {
                        required: true,
                        message: 'Please select any Win % Feed Source!',
                      },
                    ],
                  })(
                    <Select
                      disabled={feeds.length === 0}
                      loading={this.props.scoring.feedsLoading}
                      onChange={this.handlePredictSourceChange}
                    >
                      {feeds && (
                        <Option value={feeds.winFeed}>{feeds.winFeed}</Option>
                      )}
                    </Select>,
                  )}
                </Form.Item>
              </React.Fragment>
            )}
          </Form>
        </Modal>
      </Layout>
    );
  }
}

Scoring.propTypes = {
  seriesMatchesLoading: PropTypes.bool,
  scoring: PropTypes.object,
  form: PropTypes.any,
  getSeriesMatches: PropTypes.func,
  getfeaturedSeries: PropTypes.func,
  getScoring: PropTypes.func,
  resetMatches: PropTypes.func,
  saveScoring: PropTypes.func,
  getFeeds: PropTypes.func,
  history: PropTypes.object,
  feeds: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  scoring: makeSelectScoring(),
});

function mapDispatchToProps(dispatch) {
  return {
    getfeaturedSeries: () => dispatch(scoringActions.getSeries()),
    getSeriesMatches: payload =>
      dispatch(scoringActions.getSeriesMatches(payload)),
    getFeeds: payload => dispatch(scoringActions.getFeed(payload)),
    getScoring: () => dispatch(scoringActions.getScoring()),
    saveScoring: payload => dispatch(scoringActions.postScoring(payload)),
    resetMatches: () => dispatch(scoringActions.resetMatches()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'scoring', reducer });
const withSaga = injectSaga({ key: 'scoring', saga });

export default Form.create()(
  compose(
    withReducer,
    withSaga,
    withConnect,
  )(Scoring),
);
