import { takeLatest, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import {
  API_GET_SERIES,
  API_GET_FEED_ORIGIN,
  API_GET_MATCHES_BY_SERIES,
  API_GET_SCORING_DATA,
  API_POST_SCORING_DATA,
} from 'utils/requestUrl';
import * as scoringConstants from './constants';
import * as scoringActions from './actions';

export function* fetchSeries() {
  try {
    const options = yield call(request, API_GET_SERIES);
    yield put(
      scoringActions.getSeriesSuccess({
        data: options.data,
      }),
    );
  } catch (err) {
    yield put(scoringActions.getSeriesFailure(err));
  }
}

export function* fetchSeriesMatches(payload) {
  try {
    const options = yield call(
      request,
      API_GET_MATCHES_BY_SERIES(payload.payload),
    );
    yield put(
      scoringActions.getSeriesMatchesSuccess({
        data: options.data.data,
      }),
    );
  } catch (err) {
    yield put(scoringActions.getSeriesMatchesFailure(err));
  }
}

export function* fetchFeeds(payload) {
  try {
    const options = yield call(request, API_GET_FEED_ORIGIN(payload.payload));
    yield put(
      scoringActions.getFeedSuccess({
        data: options.data,
      }),
    );
  } catch (err) {
    yield put(scoringActions.getFeedFailure(err));
  }
}

export function* fetchScoring() {
  try {
    const options = yield call(request, API_GET_SCORING_DATA);
    yield put(
      scoringActions.getScoringSuccess({
        data: options.data,
      }),
    );
  } catch (err) {
    yield put(scoringActions.getScoringFailure(err));
  }
}

export function* postScoring(payload) {
  const config = {
    ...API_POST_SCORING_DATA,
    data: JSON.parse(JSON.stringify(payload.payload)),
  };
  try {
    const options = yield call(request, config);
    yield put(
      scoringActions.postScoringSuccess({
        data: options.data,
      }),
    );
  } catch (err) {
    yield put(scoringActions.postScoringFailure(err));
  }
}
// Individual exports for testing
export default function* defaultSaga() {
  yield takeLatest(scoringConstants.GET_SERIES, fetchSeries);
  yield takeLatest(scoringConstants.GET_SCORING, fetchScoring);
  yield takeLatest(scoringConstants.POST_SCORING, postScoring);
  yield takeLatest(scoringConstants.GET_MATCH_BY_SERIES, fetchSeriesMatches);
  yield takeLatest(scoringConstants.GET_FEED_SOURCE, fetchFeeds);
  // See example in containers/HomePage/saga.js
}
