/*
 *
 * Scoring actions
 *
 */

import { message } from 'antd';
import * as scoringConstants from './constants';

export function getSeries(payload) {
  return {
    type: scoringConstants.GET_SERIES,
    payload,
  };
}

export function getSeriesSuccess(payload) {
  return {
    type: scoringConstants.GET_SERIES_SUCCESS,
    payload,
  };
}

export function getSeriesFailure(payload) {
  return {
    type: scoringConstants.GET_SERIES_FAILURE,
    payload,
  };
}

export function getSeriesMatches(payload) {
  return {
    type: scoringConstants.GET_MATCH_BY_SERIES,
    payload,
  };
}

export function getSeriesMatchesSuccess(payload) {
  return {
    type: scoringConstants.GET_MATCH_BY_SERIES_SUCCESS,
    payload,
  };
}

export function getSeriesMatchesFailure(payload) {
  return {
    type: scoringConstants.GET_MATCH_BY_SERIES_FAILURE,
    payload,
  };
}

export function getFeed(payload) {
  return {
    type: scoringConstants.GET_FEED_SOURCE,
    payload,
  };
}

export function getFeedSuccess(payload) {
  return {
    type: scoringConstants.GET_FEED_SOURCE_SUCCESS,
    payload,
  };
}

export function getFeedFailure(payload) {
  return {
    type: scoringConstants.GET_FEED_SOURCE_FAILURE,
    payload,
  };
}

export function getScoring(payload) {
  return {
    type: scoringConstants.GET_SCORING,
    payload,
  };
}

export function getScoringSuccess(payload) {
  return {
    type: scoringConstants.GET_SCORING_SUCCESS,
    payload,
  };
}

export function getScoringFailure(payload) {
  return {
    type: scoringConstants.GET_SCORING_FAILURE,
    payload,
  };
}

export function postScoring(payload) {
  return {
    type: scoringConstants.POST_SCORING,
    payload,
  };
}

export function postScoringSuccess(payload) {
  console.log(payload);
  return {
    type: scoringConstants.POST_SCORING_SUCCESS,
    payload,
  };
}

export function postScoringFailure(payload) {
  message.error(payload.response.data.message);
  return {
    type: scoringConstants.POST_SCORING_FAILURE,
    payload,
  };
}

export function resetMatches() {
  return {
    type: scoringConstants.RESET_MATCHES,
  };
}
