/*
 *
 * Scoring reducer
 *
 */

import * as scoringConstants from './constants';

export const initialState = {
  seriesLoading: false,
  seriesMatchesLoading: false,
  feedsLoading: false,
  scoringLoading: false,
  series: [],
  matches: [],
  feeds: [],
  featuredMatches: [],
  scoringPostLoading: false,
  postScoringSuccess: false,
};

function scoringReducer(state = initialState, action) {
  switch (action.type) {
    case scoringConstants.GET_SERIES:
      return {
        ...state,
        seriesLoading: true,
      };
    case scoringConstants.GET_SERIES_SUCCESS:
      return {
        ...state,
        seriesLoading: false,
        series: action.payload.data.data,
      };
    case scoringConstants.GET_SERIES_FAILURE:
      return {
        ...state,
        seriesLoading: false,
      };
    case scoringConstants.GET_SCORING:
      return {
        ...state,
        scoringLoading: true,
        postScoringSuccess: false,
      };
    case scoringConstants.GET_SCORING_SUCCESS:
      return {
        ...state,
        scoringLoading: false,
        featuredMatches: action.payload.data.data,
      };
    case scoringConstants.GET_SCORING_FAILURE:
      return {
        ...state,
        scoringLoading: false,
      };
    case scoringConstants.POST_SCORING:
      return {
        ...state,
        scoringPostLoading: true,
        postScoringSuccess: false,
      };
    case scoringConstants.POST_SCORING_SUCCESS:
      return {
        ...state,
        scoringPostLoading: false,
        postScoringSuccess: true,
      };
    case scoringConstants.POST_SCORING_FAILURE:
      return {
        ...state,
        scoringPostLoading: false,
      };
    case scoringConstants.GET_MATCH_BY_SERIES:
      return {
        ...state,
        seriesMatchesLoading: true,
      };
    case scoringConstants.GET_MATCH_BY_SERIES_SUCCESS:
      return {
        ...state,
        seriesMatchesLoading: false,
        matches: action.payload.data,
      };
    case scoringConstants.GET_MATCH_BY_SERIES_FAILURE:
      return {
        ...state,
        seriesMatchesLoading: false,
      };
    case scoringConstants.GET_FEED_SOURCE:
      return {
        ...state,
        feedsLoading: true,
      };
    case scoringConstants.GET_FEED_SOURCE_SUCCESS:
      return {
        ...state,
        feedsLoading: false,
        feeds: action.payload.data.data,
      };
    case scoringConstants.GET_FEED_SOURCE_FAILURE:
      return {
        ...state,
        feedsLoading: false,
      };
    case scoringConstants.RESET_MATCHES:
      return {
        ...state,
        matches: [],
      };
    default:
      return state;
  }
}

export default scoringReducer;
