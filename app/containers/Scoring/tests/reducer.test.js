import scoringReducer from '../reducer';

describe('scoringReducer', () => {
  it('returns the initial state', () => {
    expect(scoringReducer(undefined, {})).toEqual({});
  });
});
