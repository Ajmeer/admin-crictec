/*
 *
 * Scoring constants
 *
 */
export const GET_SCORING = 'app/Scoring/GET_SCORING';
export const GET_SCORING_SUCCESS = 'app/Scoring/GET_SCORING_SUCCESS';
export const GET_SCORING_FAILURE = 'app/Scoring/GET_SCORING_FAILURE';

export const GET_SERIES = 'app/Scoring/GET_SERIES';
export const GET_SERIES_SUCCESS = 'app/Scoring/GET_SERIES_SUCCESS';
export const GET_SERIES_FAILURE = 'app/Scoring/GET_SERIES_FAILURE';

export const GET_MATCH_BY_SERIES = 'app/Scoring/GET_MATCH_BY_SERIES';
export const GET_MATCH_BY_SERIES_SUCCESS =
  'app/Scoring/GET_MATCH_BY_SERIES_SUCCESS';
export const GET_MATCH_BY_SERIES_FAILURE =
  'app/Scoring/GET_MATCH_BY_SERIES_FAILURE';

export const GET_FEED_SOURCE = 'app/Scoring/GET_FEED_SOURCE';
export const GET_FEED_SOURCE_SUCCESS = 'app/Scoring/GET_FEED_SOURCE_SUCCESS';
export const GET_FEED_SOURCE_FAILURE = 'app/Scoring/GET_FEED_SOURCE_FAILURE';

export const RESET_MATCHES = 'app/Scoring/RESET_MATCHES';

export const POST_SCORING = 'app/Scoring/POST_SCORING';
export const POST_SCORING_SUCCESS = 'app/Scoring/POST_SCORING_SUCCESS';
export const POST_SCORING_FAILURE = 'app/Scoring/POST_SCORING_FAILURE';
