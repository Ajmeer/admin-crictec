import teamProfileReducer from '../reducer';

describe('teamProfileReducer', () => {
  it('returns the initial state', () => {
    expect(teamProfileReducer(undefined, {})).toEqual({});
  });
});
