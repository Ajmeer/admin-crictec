/*
 *
 * TeamProfile reducer
 *
 */

import { DEFAULT_ACTION } from './constants';

export const initialState = {};

function teamProfileReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default teamProfileReducer;
