/* eslint-disable prefer-destructuring */
/**
 *
 * TeamProfile
 *
 */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
  Layout,
  Input,
  Row,
  Col,
  Form,
  Button,
  Icon,
  Upload,
  message,
  Select,
} from 'antd';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectTeamProfile from './selectors';
import reducer from './reducer';
import saga from './saga';
const { Content } = Layout;
const Search = Input.Search;
const Option = Select.Option;
// Upload button
const props = {
  name: 'file',
  action: '//jsonplaceholder.typicode.com/posts/',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      // console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
/* eslint-disable react/prefer-stateless-function */
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
export class TeamProfile extends React.Component {
  render() {
    return (
      <Layout className="layout">
        {/* =================== */}
        {/* Body Section Starts */}
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Team Profile</h1>
            <div className="page__info">
              <Form>
                <Row type="flex" align="middle" className="padding-left20">
                  <Col span={12}>
                    <Form.Item {...formItemLayout} label="Search">
                      <Search
                        placeholder="Search Player"
                        style={{ width: 250 }}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex" align="middle" className="padding-20">
                  <Col span={4}>Edit Team Details</Col>
                </Row>
                <Row type="flex" className="padding-left20">
                  <Col span={12}>
                    <Form.Item {...formItemLayout} label="Name">
                      <Input placeholder="Name" style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Year Joined">
                      <Input placeholder="Year Joined" style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Coach">
                      <Input placeholder="Coach" style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Capt">
                      <Input placeholder="Capt" style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="ICC Ranking Test">
                      <Input
                        placeholder="ICC Ranking Test"
                        style={{ width: 250 }}
                      />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Test Cap">
                      <Input placeholder="Test Cap" style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="ICC Ranking ODI">
                      <Input
                        placeholder="ICC Ranking ODI"
                        style={{ width: 250 }}
                      />
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="ICC Ranking T20">
                      <Input
                        placeholder="ICC Ranking T20"
                        style={{ width: 250 }}
                      />
                    </Form.Item>
                  </Col>

                  <Col span={7}>
                    <Row type="flex" className="margin-bottom30">
                      <Col span={10}>Section</Col>
                      <Col span={7}>
                        <Select
                          defaultValue="Cricket API"
                          suffixIcon={<Icon type="caret-down" />}
                          style={{ width: 120 }}
                        >
                          <Option value="Cricket API">Cricket API</Option>
                          <Option value="Opta C2">Opta C2</Option>
                        </Select>
                      </Col>
                    </Row>
                    <Row type="flex" className="margin-bottom30">
                      <Col span={10}>Sub Section</Col>
                      <Col span={7}>
                        <Select
                          defaultValue="Cricket API"
                          suffixIcon={<Icon type="caret-down" />}
                          style={{ width: 120 }}
                        >
                          <Option value="Cricket API">Cricket API</Option>
                          <Option value="Opta C2">Opta C2</Option>
                        </Select>
                      </Col>
                    </Row>
                    <Row type="flex" className="margin-bottom30">
                      <Col span={10}>Team Image</Col>
                      <Col span={7}>
                        <Upload {...props}>
                          <Button type="dashed">
                            Upload <Icon type="upload" />
                          </Button>
                        </Upload>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row type="flex" gutter={16} className="padding-20">
                  <Col className="gutter-row">
                    <Button type="danger">Delete Team</Button>
                  </Col>
                  <Col className="gutter-row">
                    <Button type="primary">Save</Button>
                  </Col>
                </Row>
              </Form>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

TeamProfile.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  teamprofile: makeSelectTeamProfile(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'teamProfile', reducer });
const withSaga = injectSaga({ key: 'teamProfile', saga });

export default Form.create()(
  compose(
    withReducer,
    withSaga,
    withConnect,
  )(TeamProfile),
);
