import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the teamProfile state domain
 */

const selectTeamProfileDomain = state => state.teamProfile || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by TeamProfile
 */

const makeSelectTeamProfile = () =>
  createSelector(selectTeamProfileDomain, substate => substate);

export default makeSelectTeamProfile;
export { selectTeamProfileDomain };
