/**
 *
 * Landing
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Layout, Row, Col } from 'antd';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectLanding from './selectors';
import reducer from './reducer';
import saga from './saga';
const { Content } = Layout;

/* eslint-disable react/prefer-stateless-function */
export class Landing extends React.Component {
  render() {
    return (
      <Layout className="layout">
        <Content className="main">
          <div className="page">
            <div className="page__info">
              <Layout style={{ padding: 24, background: '#fff' }}>
                <div className="console-tabs">
                  <Row gutter={24}>
                    <Col
                      span={6}
                      onClick={() => this.props.history.push('/scoring')}
                    >
                      Scoring
                    </Col>
                    <Col
                      span={6}
                      onClick={() => this.props.history.push('/homescreen')}
                    >
                      Home Page Editor
                    </Col>
                    <Col
                      span={6}
                      onClick={() => this.props.history.push('/player-profile')}
                    >
                      Player Profile
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col
                      span={6}
                      onClick={() =>
                        this.props.history.push('/stadium-profile')
                      }
                    >
                      Stadium Profile
                    </Col>
                    <Col
                      span={6}
                      onClick={() => this.props.history.push('/team')}
                    >
                      Team Profile
                    </Col>
                    <Col
                      span={6}
                      onClick={() => this.props.history.push('/polls')}
                    >
                      Polls
                    </Col>
                  </Row>
                  <Row gutter={24}>
                    <Col span={6}>Photos and Videos</Col>
                    <Col
                      span={6}
                      onClick={() =>
                        this.props.history.push('/ranking-records')
                      }
                    >
                      Rankings & Records
                    </Col>
                    <Col
                      span={6}
                      onClick={() => this.props.history.push('/notification')}
                    >
                      Notification
                    </Col>
                  </Row>
                </div>

                {/* Title Section */}
                {/* <CustomDropDown values={["Static", "Piechart"]} selectedIndex={0} /> */}
              </Layout>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

Landing.propTypes = {
  history: PropTypes.object,
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  landing: makeSelectLanding(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'landing', reducer });
const withSaga = injectSaga({ key: 'landing', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Landing);
