/**
 *
 * Player
 *
 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-console */
import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
// import 'antd/dist/antd.css';
// import './index.css';
import {
  Tabs,
  Row,
  Col,
  Button,
  Icon,
  message,
  Input,
  InputNumber,
  Upload,
  Layout,
  Form,
  Select,
  DatePicker,
} from 'antd';

import moment from 'moment';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
// import Column from 'antd/lib/table/Column';
import makeSelectPlayer from './selectors';
import reducer from './reducer';
import saga from './saga';

const Content = Layout.Content;
// Tabs
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
// function callback(key) {
//   console.log(key);
// }

// Search input
const Search = Input.Search;

// Upload button

// function getBase64(file) {
//   return new Promise((resolve, reject) => {
//     const reader = new FileReader();
//     reader.readAsDataURL(file);
//     reader.onload = () => resolve(reader.result);
//     reader.onerror = error => reject(error);
//   });
// }

function getBase64(file, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(file);
}

const props = {
  name: 'file',
  action: '//jsonplaceholder.typicode.com/posts/',
  accept:
    'application/csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet (.XLSX),application/vnd.ms-excel (.XLS)',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      // console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj);
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(
        `${
          info.file.name
        } file upload failed.File extension is .csv, .xlsx, .xls`,
      );
    }
  },
};

// Form
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
// Select box
const Option = Select.Option;

export class Player extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log(err, values);
    });
  };

  disableDate = val => val && val > moment().endOf('day');

  // checkfile = file => {
  //   const validExts = ['.xlsx', '.xls', '.csv'];
  //   let fileExt = file.value;
  //   fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
  //   if (validExts.indexOf(fileExt) < 0) {
  //     message.success('File Upload not Successful!');
  //   }
  // };

  // checkfile = file => {
  //   const isExcel =
  //     file.type === 'application/csv' ||
  //     file.type === 'application/vnd.ms-excel (.XLS)' ||
  //     file.type ===
  //       'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet (.XLSX)';
  //   if (!isExcel) {
  //     message.error('You can only upload excel files!');
  //   }
  // };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Layout className="layout">
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Player Profile</h1>
            <div className="page__info">
              <Form onSubmit={this.handleSubmit}>
                <Tabs defaultActiveKey="1">
                  <TabPane tab="Player Discovery" key="1">
                    <Row type="flex" align="middle" className="padding-20">
                      <Col span={3}>Section :</Col>
                      <Col span={20}>
                        <Select
                          defaultValue="Trending Players"
                          suffixIcon={<Icon type="caret-down" />}
                          style={{ width: 260 }}
                        >
                          <Option value="Cricket API">Trending Players</Option>
                          <Option value="Opta C2">Trending Players</Option>
                        </Select>
                      </Col>
                    </Row>
                    <Row className="padding-20">
                      {/* <Col span={2}>Search for Player</Col> */}
                      <Col span={9}>
                        <Form.Item
                          label="Search for Player"
                          {...formItemLayout}
                        >
                          <Search
                            placeholder="Search for Player"
                            // onSearch={value => console.log(value)}
                            style={{ width: 260 }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row type="flex" align="start" className="padding-left20">
                      <Col span={3}>List :</Col>
                      <Col span={4}>
                        <div className="profile__list padding-20">
                          <ul>
                            <li>
                              <span>Virat Kohli</span>
                              <Icon type="close" />
                            </li>
                            <li>
                              <span>MS Dhoni</span>
                              <Icon type="close" />
                            </li>
                            <li>
                              <span>Sachin</span>
                              <Icon type="close" />
                            </li>
                          </ul>
                        </div>
                      </Col>
                    </Row>
                    <Row type="flex" align="middle" className="padding-20">
                      <Col span={4}>
                        <Button
                          type="primary"
                          icon="plus"
                          size="default"
                          className="addSection"
                        >
                          Add Section
                        </Button>
                      </Col>
                    </Row>
                  </TabPane>
                  {/* Tab content 2 */}
                  <TabPane tab="Profile Details" key="2">
                    <Row type="flex" align="middle" className="padding-20">
                      {/* <Col span={2}>Search</Col> */}
                      <Col span={12}>
                        <Form.Item
                          label="Search for Player"
                          {...formItemLayout}
                        >
                          <Search
                            placeholder="input search text"
                            // onSearch={value => console.log(value)}
                            style={{ width: 250 }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row type="flex" align="middle" className="padding-left20">
                      <Col span={4}>Edit Player Details</Col>
                    </Row>
                    <Row type="flex" className="padding-20">
                      <Col span={12}>
                        <Form.Item label="Name" {...formItemLayout}>
                          {getFieldDecorator('name', {
                            rules: [
                              {
                                required: true,
                                message: 'Please input player name! ',
                              },
                            ],
                          })(
                            <Input placeholder="Name" style={{ width: 250 }} />,
                          )}
                        </Form.Item>
                        <Form.Item label="DOB" {...formItemLayout}>
                          {getFieldDecorator('dob', {
                            rules: [
                              {
                                required: true,
                                message: 'Please select date',
                              },
                            ],
                          })(
                            <DatePicker
                              disabledDate={this.disableDate}
                              style={{ width: 250 }}
                            />,
                          )}
                        </Form.Item>
                        <Form.Item label="Batting Style" {...formItemLayout}>
                          {getFieldDecorator('batting_style', {
                            rules: [
                              {
                                required: true,
                                message: 'Please select batting style! ',
                              },
                            ],
                          })(
                            <Select
                              placeholder="Batting Style"
                              suffixIcon={<Icon type="caret-down" />}
                              style={{ width: 250 }}
                            >
                              <Option value="Righthand Batsmen">
                                Righthand Batsman
                              </Option>
                              <Option value="Lefthand Batsmen">
                                Lefthand Batsman
                              </Option>
                            </Select>,
                          )}
                        </Form.Item>
                        <Form.Item label="Bowl Style" {...formItemLayout}>
                          {getFieldDecorator('bowl_style', {
                            rules: [
                              {
                                required: true,
                                message: 'Please input bowling style! ',
                              },
                            ],
                          })(
                            <Input
                              placeholder="Bowl Style"
                              style={{ width: 250 }}
                            />,
                          )}
                        </Form.Item>
                        <Form.Item label="Role" {...formItemLayout}>
                          {getFieldDecorator('role', {
                            rules: [
                              {
                                required: true,
                                message: 'Please input player role! ',
                              },
                            ],
                          })(
                            <Input placeholder="Role" style={{ width: 250 }} />,
                          )}
                        </Form.Item>

                        <Form.Item label="Test Cap" {...formItemLayout}>
                          {getFieldDecorator('test_cap', {
                            rules: [
                              {
                                required: true,
                                message: 'Please input test cap number! ',
                              },
                            ],
                          })(
                            <InputNumber
                              min={1}
                              max={1000}
                              placeholder="Test Cap"
                              style={{ width: 250 }}
                            />,
                          )}
                        </Form.Item>

                        <Form.Item label="ODI Jersey" {...formItemLayout}>
                          {getFieldDecorator('odi_jersey', {
                            rules: [
                              {
                                required: true,
                                message: 'Please input odi jersey number! ',
                              },
                            ],
                          })(
                            <InputNumber
                              min={1}
                              max={1000}
                              placeholder="ODI Jersey"
                              style={{ width: 250 }}
                            />,
                          )}
                        </Form.Item>
                      </Col>

                      <Col span={7}>
                        <Row
                          type="flex"
                          align="middle"
                          className="margin-bottom30"
                        >
                          <Col span={10}>Statistics Bat</Col>
                          <Col span={7}>
                            <Upload {...props} beforeUpload={this.checkfile}>
                              <Button type="dashed">
                                Upload <Icon type="upload" />
                              </Button>
                            </Upload>
                          </Col>
                        </Row>
                        <Row
                          type="flex"
                          align="middle"
                          className="margin-bottom30"
                        >
                          <Col span={10}>Statistics Bowl</Col>
                          <Col span={7}>
                            <Upload {...props} beforeUpload={this.checkfile}>
                              <Button type="dashed">
                                Upload <Icon type="upload" />
                              </Button>
                            </Upload>
                          </Col>
                        </Row>
                        <Row
                          type="flex"
                          align="middle"
                          className="margin-bottom30"
                        >
                          <Col span={10}>Player Image</Col>
                          <Col span={7}>
                            <Upload {...props} beforeUpload={this.checkfile}>
                              <Button type="dashed">
                                Upload <Icon type="upload" />
                              </Button>
                            </Upload>
                          </Col>
                        </Row>
                        <Row>
                          <Form.Item label="About Player" {...formItemLayout}>
                            <Col span={20}>
                              {getFieldDecorator('about_player', {
                                rules: [
                                  {
                                    required: true,
                                    message:
                                      'Please input some description about player',
                                  },
                                ],
                              })(
                                <TextArea
                                  name=""
                                  id=""
                                  cols={45}
                                  rows={8}
                                  placeholder="About Player"
                                />,
                              )}
                            </Col>
                          </Form.Item>
                        </Row>
                      </Col>
                    </Row>
                    <Row type="flex" gutter={16} className="padding-20">
                      <Col className="gutter-row">
                        <Button type="danger">Delete Player</Button>
                      </Col>
                      <Col className="gutter-row">
                        <Button htmlType="submit" type="primary">
                          Save
                        </Button>
                      </Col>
                    </Row>
                  </TabPane>
                </Tabs>
              </Form>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

const WrappedPlayerProfileForm = Form.create({ name: 'player_profile' })(
  Player,
);

Player.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  player: makeSelectPlayer(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'player', reducer });
const withSaga = injectSaga({ key: 'player', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(WrappedPlayerProfileForm);
