import playerReducer from '../reducer';

describe('playerReducer', () => {
  it('returns the initial state', () => {
    expect(playerReducer(undefined, {})).toEqual({});
  });
});
