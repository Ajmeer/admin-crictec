/*
 *
 * Player constants
 *
 */

export const DEFAULT_ACTION = 'app/Player/DEFAULT_ACTION';

export const ADD_PLAYER = 'app/Player/ADD_PLAYER';
export const ADD_PLAYER_SUCCESS = 'app/Player/ADD_PLAYER_SUCCESS';
export const ADD_PLAYER_FAILURE = 'app/Player/ADD_PLAYER_FAILURE';

export const UPDATE_PLAYER = 'app/Player/UPDATE_PLAYER';
export const UPDATE_PLAYER_SUCCESS = 'app/Player/UPDATE_PLAYER_SUCCESS';
export const UPDATE_PLAYER_FAILURE = 'app/Player/UPDATE_PLAYER_FAILURE';

export const DELETE_PLAYER = 'app/Player/DELETE_PLAYER';
export const DELETE_PLAYER_SUCCESS = 'app/Player/DELETE_PLAYER_SUCCESS';
export const DELETE_PLAYER_FAILURE = 'app/Player/DELETE_PLAYER_FAILURE';

export const SEARCH_PLAYER = 'app/Player/SEARCH_PLAYER';
export const SEARCH_PLAYER_SUCCESS = 'app/Player/SEARCH_PLAYER_SUCCESS';
export const SEARCH_PLAYER_FAILURE = 'app/Player/SEARCH_PLAYER_FAILURE';

export const RESET_POST = 'app/Player/RESET_POST';
