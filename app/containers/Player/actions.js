/*
 *
 * Player actions
 *
 */

import * as playerConstant from './constants';

export function defaultAction() {
  return {
    type: playerConstant.DEFAULT_ACTION,
  };
}

export function addPlayer(payload) {
  return {
    type: playerConstant.ADD_PLAYER,
    payload,
  };
}

export function addPlayerSuccess(payload) {
  return {
    type: playerConstant.ADD_PLAYER_SUCCESS,
    payload,
  };
}

export function addPlayerFailure(payload) {
  return {
    type: playerConstant.ADD_PLAYER_FAILURE,
    payload,
  };
}

export function updatePlayer(payload) {
  return {
    type: playerConstant.UPDATE_PLAYER,
    payload,
  };
}

export function updatePlayerSuccess(payload) {
  return {
    type: playerConstant.UPDATE_PLAYER_SUCCESS,
    payload,
  };
}

export function updatePlayerFailure(payload) {
  return {
    type: playerConstant.UPDATE_PLAYER_FAILURE,
    payload,
  };
}

export function deletePlayer(payload) {
  return {
    type: playerConstant.DELETE_PLAYER,
    payload,
  };
}

export function deletePlayerSuccess(payload) {
  return {
    type: playerConstant.DELETE_PLAYER_SUCCESS,
    payload,
  };
}

export function deletePlayerFailure(payload) {
  return {
    type: playerConstant.DELETE_PLAYER_FAILURE,
    payload,
  };
}

export function searchPlayer(payload) {
  return {
    type: playerConstant.SEARCH_PLAYER,
    payload,
  };
}

export function searchPlayerSuccess(payload) {
  return {
    type: playerConstant.SEARCH_PLAYER_SUCCESS,
    payload,
  };
}

export function searchPlayerFailure(payload) {
  return {
    type: playerConstant.SEARCH_PLAYER_FAILURE,
    payload,
  };
}

export function resetPost() {
  return {
    type: playerConstant.RESET_POST,
  };
}
