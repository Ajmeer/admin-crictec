import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homeScreenEditor state domain
 */

const selectHomeScreenEditorDomain = state =>
  state.homeScreenEditor || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by HomeScreenEditor
 */

const makeSelectHomeScreenEditor = () =>
  createSelector(selectHomeScreenEditorDomain, substate => substate);

export default makeSelectHomeScreenEditor;
export { selectHomeScreenEditorDomain };
