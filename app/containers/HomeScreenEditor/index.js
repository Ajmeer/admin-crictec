/* eslint-disable indent */
/**
 *
 * HomeScreenEditor
 *
 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */
import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
  Layout,
  Radio,
  Select,
  Row,
  Col,
  Button,
  Icon,
  Divider,
  Tabs,
  Form,
  Input,
} from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectHomeScreenEditor from './selectors';
import reducer from './reducer';
import saga from './saga';
const { Content } = Layout;
const Option = Select.Option;
const TabPane = Tabs.TabPane;
const id = 0;
function callback(key) {
  console.log(key);
}

export class HomeScreenEditor extends React.Component {
  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }
    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id + 1);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
      },
    };
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? 'Passengers' : ''}
        required={false}
        key={k}
      >
        {getFieldDecorator(`names[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          rules: [
            {
              required: true,
              whitespace: true,
              message: "Please input passenger's name or delete this field.",
            },
          ],
        })(
          <Input
            placeholder="passenger name"
            style={{ width: '60%', marginRight: 8 }}
          />,
        )}
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={keys.length === 1}
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));
    return (
      <Layout className="layout">
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Home Page Editor</h1>
            <div className="page__info">
              <Layout style={{ background: '#fff' }}>
                <div className="home_screen">
                  <Tabs defaultActiveKey="1" onChange={callback}>
                    <TabPane tab="Desktop" key="1">
                      <div className="match_layout">
                        <div className="match_layout_left">
                          <h3>Left Layout</h3>
                          <div className="mar-20">
                            <Radio>Featured Match</Radio>
                            <Radio>Featured News</Radio>
                          </div>
                          <div className="select_area mar-20">
                            <Select defaultValue="lucy" style={{ width: 170 }}>
                              <Option value="jack">Jack</Option>
                              <Option value="lucy">Lucy</Option>
                              <Option value="disabled" disabled>
                                Disabled
                              </Option>
                              <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                          </div>
                          <div className="homeScreen_table mar-20">
                            <Row className="mar-10" gutter={8}>
                              <Col span={2}>
                                <span>1</span>
                              </Col>
                              <Col span={6}>
                                <Select
                                  defaultValue="Analysis"
                                  style={{ width: 170 }}
                                >
                                  <Option value="jack">Jack</Option>
                                  <Option value="lucy">Lucy</Option>
                                  <Option value="disabled" disabled>
                                    Disabled
                                  </Option>
                                  <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                              </Col>
                            </Row>
                            <Row className="mar-10" gutter={8}>
                              <Col span={2}>
                                <span>2</span>
                              </Col>
                              <Col span={6}>
                                <Select
                                  defaultValue="News and opinion"
                                  style={{ width: 170 }}
                                >
                                  <Option value="jack">Jack</Option>
                                  <Option value="lucy">Lucy</Option>
                                  <Option value="disabled" disabled>
                                    Disabled
                                  </Option>
                                  <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                              </Col>
                            </Row>
                            <Row className="mar-10" gutter={8}>
                              <Col span={2}>
                                <span>3</span>
                              </Col>
                              <Col span={6}>
                                <Select
                                  defaultValue="Match Reports"
                                  style={{ width: 170 }}
                                >
                                  <Option value="jack">Jack</Option>
                                  <Option value="lucy">Lucy</Option>
                                  <Option value="disabled" disabled>
                                    Disabled
                                  </Option>
                                  <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                              </Col>
                            </Row>
                          </div>
                          <Form onSubmit={this.handleSubmit}>
                            {formItems}
                            <Form.Item {...formItemLayoutWithOutLabel}>
                              <Button
                                type="primary"
                                onClick={this.add}
                                className="addSection"
                              >
                                <Icon type="plus" /> Add Section
                              </Button>
                            </Form.Item>
                            {/* <Form.Item {...formItemLayoutWithOutLabel}>
                          <Button type="primary" htmlType="submit">
                            Save
                          </Button>
                        </Form.Item> */}
                          </Form>
                        </div>

                        <Divider type="vertical" />
                        <div className="match_layout_Right">
                          <h3>Right Layout</h3>
                          <div className="homeScreen_table mar-20">
                            <Row className="mar-10" gutter={8}>
                              <Col span={2}>
                                <span>1</span>
                              </Col>
                              <Col span={6}>
                                <Select
                                  defaultValue="Ranking Component"
                                  style={{ width: 170 }}
                                >
                                  <Option value="jack">Jack</Option>
                                  <Option value="lucy">Lucy</Option>
                                  <Option value="disabled" disabled>
                                    Disabled
                                  </Option>
                                  <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                              </Col>
                            </Row>
                            <Row className="mar-10" gutter={8}>
                              <Col span={2}>
                                <span>2</span>
                              </Col>
                              <Col span={6}>
                                <Select
                                  defaultValue="Feature Player"
                                  style={{ width: 170 }}
                                >
                                  <Option value="jack">Jack</Option>
                                  <Option value="lucy">Lucy</Option>
                                  <Option value="disabled" disabled>
                                    Disabled
                                  </Option>
                                  <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                              </Col>
                            </Row>
                            <Row className="mar-10" gutter={8}>
                              <Col span={2}>
                                <span>3</span>
                              </Col>
                              <Col span={6}>
                                <Select
                                  defaultValue="Trending Grounds"
                                  style={{ width: 170 }}
                                >
                                  <Option value="jack">Jack</Option>
                                  <Option value="lucy">Lucy</Option>
                                  <Option value="disabled" disabled>
                                    Disabled
                                  </Option>
                                  <Option value="Yiminghe">yiminghe</Option>
                                </Select>
                              </Col>
                            </Row>
                          </div>
                          <Form onSubmit={this.handleSubmit}>
                            {formItems}
                            <Form.Item {...formItemLayoutWithOutLabel}>
                              <Button
                                type="primary"
                                onClick={this.add}
                                className="addSection"
                              >
                                <Icon type="plus" /> Add Section
                              </Button>
                            </Form.Item>
                            {/* <Form.Item {...formItemLayoutWithOutLabel}>
                          <Button type="primary" htmlType="submit">
                            Save
                          </Button>
                        </Form.Item> */}
                          </Form>
                        </div>
                      </div>
                    </TabPane>
                    <TabPane tab="Mobile App" key="2">
                      <div className="homeScreen_mobile">
                        <div className="homeScreen_table mar-20">
                          <Row className="mar-10" gutter={8}>
                            <Col span={2}>
                              <span>1</span>
                            </Col>
                            <Col span={6}>
                              <Select
                                defaultValue="Analysis"
                                style={{ width: 170 }}
                              >
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>
                                  Disabled
                                </Option>
                                <Option value="Yiminghe">yiminghe</Option>
                              </Select>
                            </Col>
                          </Row>
                          <Row className="mar-10" gutter={8}>
                            <Col span={2}>
                              <span>2</span>
                            </Col>
                            <Col span={6}>
                              <Select
                                defaultValue="Featured Match"
                                style={{ width: 170 }}
                              >
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>
                                  Disabled
                                </Option>
                                <Option value="Yiminghe">yiminghe</Option>
                              </Select>
                            </Col>
                          </Row>
                          <Row className="mar-10" gutter={8}>
                            <Col span={2}>
                              <span>3</span>
                            </Col>
                            <Col span={6}>
                              <Select
                                defaultValue="News and opinion"
                                style={{ width: 170 }}
                              >
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>
                                  Disabled
                                </Option>
                                <Option value="Yiminghe">yiminghe</Option>
                              </Select>
                            </Col>
                          </Row>
                          <Row className="mar-10" gutter={8}>
                            <Col span={2}>
                              <span>4</span>
                            </Col>
                            <Col span={6}>
                              <Select
                                defaultValue="Match Reports"
                                style={{ width: 170 }}
                              >
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>
                                  Disabled
                                </Option>
                                <Option value="Yiminghe">yiminghe</Option>
                              </Select>
                            </Col>
                          </Row>
                        </div>
                        <Form onSubmit={this.handleSubmit}>
                          {formItems}
                          <Form.Item {...formItemLayoutWithOutLabel}>
                            <Button
                              type="primary"
                              onClick={this.add}
                              className="addSection"
                            >
                              <Icon type="plus" /> Add Section
                            </Button>
                          </Form.Item>
                          {/* <Form.Item {...formItemLayoutWithOutLabel}>
                          <Button type="primary" htmlType="submit">
                            Save
                          </Button>
                        </Form.Item> */}
                        </Form>
                      </div>
                    </TabPane>
                  </Tabs>
                </div>
                {/* Title Section */}
                {/* <CustomDropDown values={["Static", "Piechart"]} selectedIndex={0} /> */}
              </Layout>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

HomeScreenEditor.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  homescreeneditor: makeSelectHomeScreenEditor(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'homeScreenEditor', reducer });
const withSaga = injectSaga({ key: 'homeScreenEditor', saga });

export default Form.create()(
  compose(
    withReducer,
    withSaga,
    withConnect,
  )(HomeScreenEditor),
);
