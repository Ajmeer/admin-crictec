import homeScreenEditorReducer from '../reducer';

describe('homeScreenEditorReducer', () => {
  it('returns the initial state', () => {
    expect(homeScreenEditorReducer(undefined, {})).toEqual({});
  });
});
