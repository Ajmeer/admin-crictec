import rankingRecordsReducer from '../reducer';

describe('rankingRecordsReducer', () => {
  it('returns the initial state', () => {
    expect(rankingRecordsReducer(undefined, {})).toEqual({});
  });
});
