/**
 *
 * RankingRecords
 *
 */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Layout, Upload, Row, Col, Icon, message, Button } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectRankingRecords from './selectors';
import reducer from './reducer';
import saga from './saga';

const { Content } = Layout;

// Upload button
const props = {
  name: 'file',
  action: '//jsonplaceholder.typicode.com/posts/',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      // console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
export class RankingRecords extends React.Component {
  render() {
    return (
      <Layout className="layout">
        {/* =================== */}
        {/* Body Section Starts */}
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Ranking & Records</h1>
            <div className="page__info">
              <Row
                className="margin-bottom20"
                style={{ width: '50%', marginLeft: '10%', marginTop: '5%' }}
              >
                <Col span={3}>Ranking</Col>
                <Col span={3} offset={4}>
                  <Upload {...props}>
                    <Button type="dashed">
                      Upload <Icon type="upload" />
                    </Button>
                  </Upload>
                </Col>
              </Row>
              <Row style={{ width: '50%', marginLeft: '10%', marginTop: '5%' }}>
                <Col span={3}>Records</Col>
                <Col span={3} offset={4}>
                  <Upload {...props}>
                    <Button type="dashed">
                      Upload <Icon type="upload" />
                    </Button>
                  </Upload>
                </Col>
              </Row>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

RankingRecords.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  rankingrecords: makeSelectRankingRecords(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'rankingRecords', reducer });
const withSaga = injectSaga({ key: 'rankingRecords', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RankingRecords);
