import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the rankingRecords state domain
 */

const selectRankingRecordsDomain = state =>
  state.rankingRecords || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by RankingRecords
 */

const makeSelectRankingRecords = () =>
  createSelector(selectRankingRecordsDomain, substate => substate);

export default makeSelectRankingRecords;
export { selectRankingRecordsDomain };
