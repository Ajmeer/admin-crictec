import { createSelector } from 'reselect';
import photosReducer from './reducer';

/**
 * Direct selector to the photos state domain
 */

const selectPhotosDomain = state => state.photos || photosReducer;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Photos
 */

const makeSelectPhotos = () =>
  createSelector(selectPhotosDomain, substate => substate);

export default makeSelectPhotos;
export { selectPhotosDomain };
