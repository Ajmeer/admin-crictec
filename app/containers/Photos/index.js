/* eslint-disable prefer-destructuring */
/**
 *
 * Photos
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
  Button,
  Upload,
  Layout,
  Form,
  Select,
  Icon,
  Row,
  Col,
  Input,
} from 'antd';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectPhotos from './selectors';
import reducer from './reducer';
import saga from './saga';
const Content = Layout.Content;
const Option = Select.Option;
const { TextArea } = Input;

/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */
export class Photos extends React.Component {
  render() {
    return (
      <Layout className="layout">
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Photos</h1>
            <div className="page__info">
              <Layout style={{ padding: 24, background: '#fff' }}>
                <div>
                  <Form>
                    <Form.Item>
                      <Row type="flex" align="middle" className="padding-20">
                        <Col span={3}>Type :</Col>
                        <Col span={20}>
                          <Select
                            defaultValue="Select"
                            suffixIcon={<Icon type="caret-down" />}
                            style={{ width: 260 }}
                          >
                            <Option value="Cricket API">Image</Option>
                            <Option value="Opta C2">Video</Option>
                          </Select>
                        </Col>
                      </Row>
                    </Form.Item>
                    <Form.Item>
                      <Row type="flex" align="middle" className="padding-20">
                        <Col span={3}>Title :</Col>
                        <Col span={20}>
                          <Input placeholder="Title" style={{ width: 250 }} />
                        </Col>
                      </Row>
                    </Form.Item>
                    <Form.Item>
                      <Row type="flex" align="middle" className="padding-20">
                        <Col span={3}>Upload :</Col>
                        <Col span={20}>
                          <Upload>
                            <Button>
                              <Icon type="upload" /> Upload
                            </Button>
                          </Upload>
                        </Col>
                      </Row>
                    </Form.Item>
                    <Form.Item>
                      <Row type="flex" align="middle" className="padding-20">
                        <Col span={3}>Discription :</Col>
                        <Col span={20}>
                          <TextArea
                            placeholder="Discription"
                            autosize={{ minRows: 2, maxRows: 6 }}
                            style={{ width: 250 }}
                          />
                        </Col>
                      </Row>
                    </Form.Item>
                    <Form.Item>
                      <Row type="flex" align="middle" className="padding-20">
                        <Col span={3}>Video URL :</Col>
                        <Col span={20}>
                          <Input placeholder="video" style={{ width: 250 }} />
                        </Col>
                      </Row>
                    </Form.Item>
                    <Form.Item>
                      <Row type="flex" align="middle" className="padding-20">
                        <Button type="primary" htmlType="submit">
                          Send
                        </Button>
                      </Row>
                    </Form.Item>
                  </Form>
                </div>
              </Layout>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

Photos.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  photos: makeSelectPhotos(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'photos', reducer });
const withSaga = injectSaga({ key: 'photos', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Photos);
