import stadiumReducer from '../reducer';

describe('stadiumReducer', () => {
  it('returns the initial state', () => {
    expect(stadiumReducer(undefined, {})).toEqual({});
  });
});
