import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the stadium state domain
 */

const selectStadiumDomain = state => state.stadium || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Stadium
 */

const makeSelectStadium = () =>
  createSelector(selectStadiumDomain, substate => substate);

export default makeSelectStadium;
export { selectStadiumDomain };
