/**
 *
 * Stadium
 *
 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-console */
import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
  Tabs,
  Row,
  Col,
  Button,
  Icon,
  message,
  Input,
  Upload,
  Layout,
  Form,
  Select,
} from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectStadium from './selectors';
import reducer from './reducer';
import saga from './saga';
const Content = Layout.Content;
// Tabs
const TabPane = Tabs.TabPane;

function callback(key) {
  console.log(key);
}
// Search input
const Search = Input.Search;

// Upload button
const props = {
  name: 'file',
  action: '//jsonplaceholder.typicode.com/posts/',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
// Form
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
// Select box
const Option = Select.Option;
export class Stadium extends React.Component {
  render() {
    return (
      <Layout className="layout">
        <Content className="main">
          <div className="page">
            <h1 className="main__title">Stadium Profile</h1>
            <div className="page__info">
              <Tabs defaultActiveKey="1" onChange={callback}>
                <TabPane tab="Stadium Discovery" key="1">
                  <Row type="flex" align="middle" className="padding-20">
                    <Col span={3}>Section</Col>
                    <Col span={20}>
                      <Select
                        defaultValue="Trending Stadium"
                        suffixIcon={<Icon type="caret-down" />}
                        style={{ width: 260 }}
                      >
                        <Option value="Cricket API">Trending Stadium</Option>
                        <Option value="Opta C2">Trending Stadium</Option>
                      </Select>
                    </Col>
                  </Row>
                  <Row className="padding-20">
                    {/* <Col span={2}>Search for Stadium</Col> */}
                    <Col span={9}>
                      <Form.Item label="Search for Player" {...formItemLayout}>
                        <Search
                          placeholder="input search text"
                          // onSearch={value => console.log(value)}
                          style={{ width: 260 }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row type="flex" align="start" className="padding-left20">
                    <Col span={3}>List</Col>
                    <Col span={4}>
                      <div className="profile__list padding-20">
                        <ul>
                          <li>
                            <span>MCG</span>
                            <Icon type="close" />
                          </li>
                          <li>
                            <span>SCG</span>
                            <Icon type="close" />
                          </li>
                          <li>
                            <span>MA Chidambaram</span>
                            <Icon type="close" />
                          </li>
                        </ul>
                      </div>
                    </Col>
                  </Row>
                  <Row type="flex" align="middle" className="padding-20">
                    <Col span={4}>
                      <Button
                        type="primary"
                        icon="plus"
                        size="default"
                        className="addSection"
                      >
                        Add Section
                      </Button>
                    </Col>
                  </Row>
                </TabPane>
                {/* Tab content 2 */}
                <TabPane tab="Stadium Details" key="2">
                  <Row type="flex" align="middle" className="padding-20">
                    {/* <Col span={2}>Search</Col> */}
                    <Col span={12}>
                      <Form.Item label="Search for Player" {...formItemLayout}>
                        <Search
                          placeholder="Search for Stadium"
                          // onSearch={value => console.log(value)}
                          style={{ width: 260 }}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row type="flex" align="middle" className="padding-20">
                    <Col span={4}>Stadium Details</Col>
                  </Row>
                  <Row type="flex" className="padding-20">
                    <Col span={12}>
                      <Form.Item label="Name" {...formItemLayout}>
                        <Input placeholder="Name" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="City" {...formItemLayout}>
                        <Input placeholder="City" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="Capacity" {...formItemLayout}>
                        <Input placeholder="Capacity" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="Floodlights" {...formItemLayout}>
                        <Input
                          placeholder="Floodlights"
                          style={{ width: 250 }}
                        />
                      </Form.Item>
                      <Form.Item label="Bowl Ends" {...formItemLayout}>
                        <Input placeholder="Bowl Ends" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="Domestic Teams" {...formItemLayout}>
                        <Input
                          placeholder="Domestic Teams"
                          style={{ width: 250 }}
                        />
                      </Form.Item>
                      <Form.Item label="1st Test" {...formItemLayout}>
                        <Input placeholder="1st Test" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="1st ODI" {...formItemLayout}>
                        <Input placeholder="1st ODI" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="1st ODI" {...formItemLayout}>
                        <Input placeholder="1st ODI" style={{ width: 250 }} />
                      </Form.Item>
                      <Form.Item label="1st T20" {...formItemLayout}>
                        <Input placeholder="1st T20" style={{ width: 250 }} />
                      </Form.Item>
                    </Col>

                    <Col span={7}>
                      <Row
                        type="flex"
                        align="middle"
                        className="margin-bottom30"
                      >
                        <Col span={10}>Statistics Test</Col>
                        <Col span={7}>
                          <Upload {...props}>
                            <Button type="dashed">
                              Upload <Icon type="upload" />
                            </Button>
                          </Upload>
                        </Col>
                      </Row>
                      <Row
                        type="flex"
                        align="middle"
                        className="margin-bottom30"
                      >
                        <Col span={10}>Statistics ODI</Col>
                        <Col span={7}>
                          <Upload {...props}>
                            <Button type="dashed">
                              Upload <Icon type="upload" />
                            </Button>
                          </Upload>
                        </Col>
                      </Row>
                      <Row
                        type="flex"
                        align="middle"
                        className="margin-bottom30"
                      >
                        <Col span={10}>Statistics T20</Col>
                        <Col span={7}>
                          <Upload {...props}>
                            <Button type="dashed">
                              Upload <Icon type="upload" />
                            </Button>
                          </Upload>
                        </Col>
                      </Row>
                      <Row
                        type="flex"
                        align="middle"
                        className="margin-bottom30"
                      >
                        <Col span={10}>Stadium Image</Col>
                        <Col span={7}>
                          <Upload {...props}>
                            <Button type="dashed">
                              Upload <Icon type="upload" />
                            </Button>
                          </Upload>
                        </Col>
                      </Row>
                      <Row
                        type="flex"
                        align="middle"
                        className="margin-bottom30"
                      >
                        <Col span={10}>Stadium Thumbnail</Col>
                        <Col span={7}>
                          <Upload {...props}>
                            <Button type="dashed">
                              Upload <Icon type="upload" />
                            </Button>
                          </Upload>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={10}>About Stadium</Col>
                        <Col span={10}>
                          <textarea
                            name=""
                            id=""
                            cols="45"
                            rows="8"
                            placeholder="About Stadium"
                          />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <Row type="flex" gutter={16} className="padding-20">
                    <Col className="gutter-row">
                      <Button type="danger">Delete Stadium</Button>
                    </Col>
                    <Col className="gutter-row">
                      <Button type="primary">Save</Button>
                    </Col>
                  </Row>
                </TabPane>
              </Tabs>
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

Stadium.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  stadium: makeSelectStadium(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'stadium', reducer });
const withSaga = injectSaga({ key: 'stadium', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Stadium);
