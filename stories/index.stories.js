// import React from 'react';

import { configure } from '@storybook/react';
// // import { linkTo } from '@storybook/addon-links';

import '../app/assets/scss/main.scss';

const req = require.context('../app', true, /stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

// initialize react-storybook
configure(loadStories, module);
