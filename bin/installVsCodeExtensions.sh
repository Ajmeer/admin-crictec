#!/bin/bash
echo -e "\nChecking if Visual studio code command is installed:"
if ! [ -x "$(command -v code)" ]; then
  echo -e '\nError: code command not found.'
  echo -e "\n1. In VS Code press CMD+SHIFT+P"
  echo -e "\n2. Search and select \"Install 'code' command in PATH\""
  echo -e "\nDo npm install again."
  exit 0
fi

echo -e "Installing recommended vscode extensions:"
echo -e "\nESLint"
code --install-extension dbaeumer.vscode-eslint --force
echo -e "\nPrettier - Code formatter"
code --install-extension esbenp.prettier-vscode --force
echo -e "\nEditorConfig for VS Code"
code --install-extension EditorConfig.EditorConfig --force
echo -e "\nSnippets for react"
code --install-extension walter-ribeiro.full-react-snippets --force
echo -e "\nHTML to JSX Conversion"
code --install-extension riazxrazor.html-to-jsx --force
echo -e "\nImport Cost"
code --install-extension wix.vscode-import-cost --force
echo -e "\nReact Pure to class component"
code --install-extension angryobject.react-pure-to-class-vscode --force
