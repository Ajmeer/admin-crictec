# React Scaffolding

### A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices

## Features

> **Quick scaffolding**: Create components, containers, routes, selectors and sagas - and their tests - right from the CLI!


> **Instant feedback**: Enjoy the best DX (Developer eXperience) and code your app at the speed of thought! Your saved changes to the CSS and JS are reflected instantaneously without refreshing the page. Preserve application state even when you update something in the underlying code!


> **Predictable state management**: Unidirectional data flow allows for change logging and time travel debugging.


> **Next generation JavaScript**: Use template strings, object destructuring, arrow functions, JSX syntax and more.


> **Industry-standard routing**: It's natural to want to add pages (e.g. `/about`) to your application, and routing makes this possible.


> **Industry-standard i18n internationalization support**: Scalable apps need to support multiple languages, easily add and support multiple languages with `react-intl`.


> **Offline-first**: The next frontier in performant web apps: availability without a network connection from the instant your users load the app.


> **Static code analysis**: Focus on writing new features without worrying about formatting or code quality. With the right editor setup, your code will automatically be formatted and linted as you work.


> **SEO**: We support SEO (document head tags management) for search engines that support indexing of JavaScript content. (eg. Google)


> **Storybook**: Storybook is a development environment for UI components. It allows you to browse a component library, view the different states of each component, and interactively develop and test components.


But wait... there's more!

- _The best test setup:_ Automatically guarantee code quality and non-breaking
  changes. (Seen a react app with 100% test coverage before?)
- _Native web app:_ Your app's new home? The home screen of your users' phones.
- _The fastest fonts:_ Say goodbye to vacant text.
- _Stay fast:_ Profile your app's performance from the comfort of your command
  line!
- _Catch problems:_ AppVeyor and TravisCI setups included by default, so your
  tests get run automatically on Windows and Unix.

## Quick start

1.  Make sure that you have Node.js v8.10 and npm v5 or above installed.
2.  Clone this repo
3.  Move to the appropriate directory: `cd <YOUR_PROJECT_NAME>`.
4.  Run `npm run setup` in order to install dependencies and clean the git repo.
    _At this point you can run `npm start` to see the example app at `http://localhost:3000`._
5.  For storybook run `npm run storybook`

Now you're ready to rumble!
